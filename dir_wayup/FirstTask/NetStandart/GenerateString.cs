﻿using System;
using System.Collections.Generic;
using Microsoft.Extensions.Configuration;

namespace NetStandart
{
    public class GenerateString
    {
        public static string Method(string args)
        {
            string output = $"{DateTime.Now} Hello, {args}!";

            return output;
        }
    }
}
