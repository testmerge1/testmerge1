﻿using System;
using Gtk;

public partial class MainWindow : Gtk.Window
{
    public MainWindow() : base(Gtk.WindowType.Toplevel)
    {
        Build();
    }

    protected void OnDeleteEvent(object sender, DeleteEventArgs a)
    {
        Application.Quit();
        a.RetVal = true;
    }

    protected void NewClick(object sender, EventArgs e)
    {
        if (!string.IsNullOrWhiteSpace(this.entry11.Text))
        {
            var dialog = new MessageDialog(this, DialogFlags.Modal, MessageType.Info, ButtonsType.Ok, "{0}", NetStandart.GenerateString.Method(entry11.Text));

            dialog.Run();
            dialog.Destroy();
        }
    }
}
