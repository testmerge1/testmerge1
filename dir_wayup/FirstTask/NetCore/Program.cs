﻿using System;
using System.Collections.Generic;
using Microsoft.Extensions.Configuration;

namespace NetCore
{
    class Program
    {
        static void Main(string[] args)
        {
            var builder = new ConfigurationBuilder();
            builder.AddCommandLine(args, new Dictionary<string, string>
            {
                ["-Parameter"] = "Parameter"
            });

            var config = builder.Build();
            var parameter = config["Parameter"];

            Console.WriteLine($"{NetStandart.GenerateString.Method(parameter)}!");
        }
    }
}
