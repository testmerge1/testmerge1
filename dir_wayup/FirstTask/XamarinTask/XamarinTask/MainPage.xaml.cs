﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace XamarinTask
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        private void ButtonId_OnClicked(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(EntryId.Text))
            {
                DisplayAlert("Just smile!", NetStandart.GenerateString.Method(EntryId.Text), "OK");
            }
        }
    }
}
